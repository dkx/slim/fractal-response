.PHONY: install-deps
install-deps:
	docker run --rm -v `pwd`:/app -w /app composer install

.PHONY: phpstan
phpstan:
	docker run --rm -v `pwd`:/app -w /app php:7.3.4-alpine vendor/bin/phpstan analyze --no-progress -c phpstan.neon
