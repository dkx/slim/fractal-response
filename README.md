# DKX/Slim/FractalResponse

## Installation

```bash
$ composer require dkx/slim-fractal-response
```

## Usage

**Register middleware:**

```php
<?php

use DKX\SlimFractalResponse\FractalResponseMiddleware;

$app->add(new FractalResponseMiddleware($fractalManager));
```

**Use in route:**

```php
<?php

use DKX\SlimFractalResponse\CollectionResponse;
use DKX\SlimFractalResponse\ItemResponse;
use Slim\Http\Request;
use Slim\Http\Response;

$app->get('/users', function (Request $request, Response $response): Response {
	return new CollectionResponse($response, $userTransformer, $users);
});

$app->get('/users/{id}', function (Request $request, Response $response): Response {
	return new ItemResponse($response, $userTransformer, $user);
});
```
