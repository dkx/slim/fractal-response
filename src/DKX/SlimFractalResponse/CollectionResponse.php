<?php

declare(strict_types=1);

namespace DKX\SlimFractalResponse;

use League\Fractal\Resource\Collection;
use League\Fractal\Resource\ResourceAbstract;
use League\Fractal\TransformerAbstract;

class CollectionResponse extends WrappedResponse
{


	protected function createFractalResource(TransformerAbstract $transformer, $data, ?string $resourceKey = null): ResourceAbstract
	{
		return new Collection($data, $transformer, $resourceKey);
	}

}
