<?php

declare(strict_types=1);

namespace DKX\SlimFractalResponse\Exception;

use RuntimeException;

final class OpenResourceFailedException extends RuntimeException
{

}
