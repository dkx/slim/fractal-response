<?php

declare(strict_types=1);

namespace DKX\SlimFractalResponse;

use League\Fractal\Manager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class FractalResponseMiddleware
{


	/** @var \League\Fractal\Manager */
	private $manager;


	public function __construct(Manager $manager)
	{
		$this->manager = $manager;
	}


	public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next): ResponseInterface
	{
		$response = $next($request, $response);

		if ($response instanceof WrappedResponse) {
			$response = $response->unwrapResponse($this->manager);
		}

		return $response;
	}

}
