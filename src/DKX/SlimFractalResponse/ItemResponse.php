<?php

declare(strict_types=1);

namespace DKX\SlimFractalResponse;

use League\Fractal\Resource\Item;
use League\Fractal\Resource\ResourceAbstract;
use League\Fractal\TransformerAbstract;

class ItemResponse extends WrappedResponse
{


	protected function createFractalResource(TransformerAbstract $transformer, $data, ?string $resourceKey = null): ResourceAbstract
	{
		return new Item($data, $transformer, $resourceKey);
	}

}
