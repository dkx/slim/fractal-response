<?php

declare(strict_types=1);

namespace DKX\SlimFractalResponse;

use DKX\SlimFractalResponse\Exception\OpenResourceFailedException;
use League\Fractal\Manager;
use League\Fractal\Resource\ResourceAbstract;
use League\Fractal\TransformerAbstract;
use Nette\Utils\Json;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Slim\Http\Stream;

abstract class WrappedResponse implements ResponseInterface
{


	/** @var \Psr\Http\Message\ResponseInterface */
	private $response;

	/** @var \League\Fractal\TransformerAbstract */
	private $transformer;

	/** @var mixed */
	private $data;

	/** @var string|null */
	private $resourceKey;


	/**
	 * @param \Psr\Http\Message\ResponseInterface $response
	 * @param \League\Fractal\TransformerAbstract $transformer
	 * @param mixed $data
	 * @param string|null $resourceKey
	 */
	public function __construct(ResponseInterface $response, TransformerAbstract $transformer, $data, ?string $resourceKey = null)
	{
		$this->response = $response;
		$this->transformer = $transformer;
		$this->data = $data;
		$this->resourceKey = $resourceKey;
	}


	public function unwrapResponse(Manager $manager): ResponseInterface
	{
		$resource = $this->createFractalResource($this->transformer, $this->data, $this->resourceKey);
		$data = $manager->createData($resource)->toArray();

		return $this->sendJson($data);
	}


	public function getProtocolVersion()
	{
		return $this->response->getProtocolVersion();
	}


	public function withProtocolVersion($version)
	{
		return $this->response->withProtocolVersion($version);
	}


	public function getHeaders()
	{
		return $this->response->getHeaders();
	}


	public function hasHeader($name)
	{
		return $this->response->hasHeader($name);
	}


	public function getHeader($name)
	{
		return $this->response->getHeader($name);
	}


	public function getHeaderLine($name)
	{
		return $this->response->getHeaderLine($name);
	}


	public function withHeader($name, $value)
	{
		return $this->response->withHeader($name, $value);
	}


	public function withAddedHeader($name, $value)
	{
		return $this->response->withAddedHeader($name, $value);
	}


	public function withoutHeader($name)
	{
		return $this->response->withoutHeader($name);
	}


	public function getBody()
	{
		return $this->response->getBody();
	}


	public function withBody(StreamInterface $body)
	{
		return $this->response->withBody($body);
	}


	public function getStatusCode()
	{
		return $this->response->getStatusCode();
	}


	public function withStatus($code, $reasonPhrase = '')
	{
		return $this->response->withStatus($code, $reasonPhrase);
	}


	public function getReasonPhrase()
	{
		return $this->response->getReasonPhrase();
	}


	/**
	 * @param \League\Fractal\TransformerAbstract $transformer
	 * @param mixed $data
	 * @param string|null $resourceKey
	 * @return \League\Fractal\Resource\ResourceAbstract
	 */
	abstract protected function createFractalResource(TransformerAbstract $transformer, $data, ?string $resourceKey = null): ResourceAbstract;


	/**
	 * @param mixed[] $data
	 * @throws \Nette\Utils\JsonException
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	protected function sendJson(array $data): ResponseInterface
	{
		$body = $this->createJsonStream($data);

		$response = $this->response;
		$response = $response->withBody($body);
		$response = $response->withHeader('Content-Type', 'application/json;charset=utf-8');

		return $response;
	}


	/**
	 * @param mixed[] $data
	 * @return \Slim\Http\Stream
	 * @throws \Nette\Utils\JsonException
	 */
	private function createJsonStream(array $data): Stream
	{
		$stream = fopen('php://memory', 'r+');

		if ($stream === false) {
			throw new OpenResourceFailedException;
		}

		fwrite($stream, Json::encode($data));
		rewind($stream);

		return new Stream($stream);
	}

}
